const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const app = express();

// Get port from environment variable
const port = process.env.PORT ?? 3000;

// Use environment variable for server URL
const SERVER_URL = process.env.SERVER_URL ?? 'localhost';

app.use(bodyParser.json());

app.get('/getCodeState', async (req, res) => {
  try {
    const response = await fetch(`${SERVER_URL}/getCodeState`);
    const data = await response.json();
    res.json(data);
  } catch (error) {
    console.error('Error fetching code state:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/getBranch', async (req, res) => {
  try {
    const response = await fetch(`${SERVER_URL}/getBranch`);
    const data = await response.json();
    res.json(data);
  } catch (error) {
    console.error('Error fetching branch information:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/sendChanges', async (req, res) => {
  try {
    const response = await fetch(`${SERVER_URL}/sendChanges`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(req.body),
    });

    if (!response.ok) {
      throw new Error('Failed to send changes');
    }

    res.send('Changes sent successfully');
  } catch (error) {
    console.error('Error sending changes:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(`Relay server listening at http://localhost:${port}`);
});