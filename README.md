
# Relay Server

This repository contains the code for a relay server that handles requests for fetching code state and branch information, and sending changes. The server is set up to run in a Docker container and is integrated with Nginx Proxy Manager for easy access and management.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Setup](#setup)
- [Configuration](#configuration)
- [Usage](#usage)
- [License](#license)

## Prerequisites

- Docker
- Docker Compose
- Nginx Proxy Manager

## Setup

### 1. Clone the Repository

\`\`\`sh
git clone https://github.com/your-username/relay-server.git
cd relay-server
\`\`\`

### 2. Directory Structure

Ensure your directory structure looks like this:

\`\`\`
/project-root
  /src
    package.json
    server.js
  docker-compose.yml
\`\`\`

### 3. Build and Run the Docker Container

From the project root directory, run:

\`\`\`sh
docker-compose up -d
\`\`\`

This will build the Docker image and start the relay server in a container.

### 4. Configure Nginx Proxy Manager

Add a new proxy host in Nginx Proxy Manager with the following details:
- **Domain Names**: \`yourdomain.com\`
- **Scheme**: \`http\`
- **Forward Hostname / IP**: \`localhost\`
- **Forward Port**: \`80\`

Save and apply the configuration.

## Configuration

### Docker Compose

The \`docker-compose.yml\` file is configured to use a recent Node.js image, expose port 80, and connect to an external network \`cross-net\`. Ensure your \`docker-compose.yml\` looks like this:

\`\`\`yaml
version: '3'
services:
  relay-server:
    image: node:18  # Use a recent version of Node.js
    working_dir: /app
    volumes:
      - ./src:/app  # Mount the src directory to /app in the container
    ports:
      - "80:3000"  # Map port 80 on the host to port 3000 in the container
    environment:
      SERVER_URL: 'http://your-server-url'
    command: >
      sh -c "npm install && node server.js"
    networks:
      - cross-net

networks:
  cross-net:
    external: true
\`\`\`

### Server Code

Ensure your server code is in the \`src\` directory:

#### \`src/server.js\`

\`\`\`javascript
const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const app = express();
const port = 3000;

// Use environment variable for server URL
const SERVER_URL = process.env.SERVER_URL;

app.use(bodyParser.json());

app.get('/getCodeState', async (req, res) => {
  try {
    const response = await fetch(\`\${SERVER_URL}/getCodeState\`);
    const data = await response.json();
    res.json(data);
  } catch (error) {
    console.error('Error fetching code state:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/getBranch', async (req, res) => {
  try {
    const response = await fetch(\`\${SERVER_URL}/getBranch\`);
    const data = await response.json();
    res.json(data);
  } catch (error) {
    console.error('Error fetching branch information:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.post('/sendChanges', async (req, res) => {
  try {
    const response = await fetch(\`\${SERVER_URL}/sendChanges\`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(req.body),
    });

    if (!response.ok) {
      throw new Error('Failed to send changes');
    }

    res.send('Changes sent successfully');
  } catch (error) {
    console.error('Error sending changes:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(\`Relay server listening at http://localhost:\${port}\`);
});
\`\`\`

#### \`src/package.json\`

\`\`\`json
{
  "name": "relay-server",
  "version": "1.0.0",
  "description": "Relay server for handling requests",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "dependencies": {
    "body-parser": "^1.19.0",
    "express": "^4.17.1",
    "node-fetch": "^2.6.1"
  }
}
\`\`\`

## Usage

- The relay server listens on port 80 and handles the following routes:
  - \`GET /getCodeState\`: Fetches the current code state.
  - \`GET /getBranch\`: Fetches the current branch information.
  - \`POST /sendChanges\`: Sends code changes to the server.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
